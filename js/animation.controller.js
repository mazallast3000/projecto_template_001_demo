const debounce = function(func, wait, imediate) {
    let timeout;
    return function(...args) {
        const context = this;
        const later = function() {
            timeout = null;
            if (!imediate) func.apply(context, args);
        };
        const callNow = imediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
const target = document.querySelectorAll('[data-anime]');
const animayionClass = 'animate';

function animateScroll() {
    const windowTop = window.pageYOffset + ((window.innerHeight * 3) / 4);
    target.forEach(function(element) {
        if (windowTop > element.offsetTop) {
            element.classList.add(animayionClass);
        } else {
            element.classList.remove(animayionClass);
        }

    });
}
animateScroll();

if (target.length) {

    window.addEventListener('scroll', debounce(function() {
        animateScroll();
        console.log('lolooo');

    }, 200));
}